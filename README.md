# Drain

Gem for the drainpipe.

## Installation

Add this line to your application's Gemfile:

```
#!ruby

gem 'drain', git: 'https://bitbucket.org/wordtracker/drain.git', tag: '2.0.3'

```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install drain

## Usage

### Global Configuration

```
#!ruby

Drain.config do |config|
  config.app_id   = 'APP ID'
  config.app_key = 'APP KEY'

  # no '/' at the end please
  config.url = 'http://drainpipe-api'
end

```

### Example

```
#!ruby

pipe = Drain::Pipe.new

# all options are optional
options = {
  limit: 100,
  adult: true,
  plurals: true,
  country: 'us'
}

```

#### Legacy Methods

```
#!ruby

# match request
match = pipe.match(['keyword1', 'keyword2'], options)

# phrase request
phrase = pipe.phrase(['keyword1', 'keyword2'], options)

# exact request
exact = pipe.exact(['keyword1', 'keyword2'], options)

```

#### New Methods

Soon...
