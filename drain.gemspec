# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'drain/version'

Gem::Specification.new do |spec|
  spec.name          = 'drain'
  spec.version       = Drain::VERSION
  spec.authors       = ['Alex Rowan', 'Gediminas Dackys']
  spec.email         = ['developers@wordtracker.com']
  spec.description   = 'Gem for the drainpipe'
  spec.summary       = 'Gem for the drainpipe'
  spec.homepage      = 'https://bitbucket.org/wordtracker/drain'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'webmock'
  spec.add_development_dependency 'mocha'

  spec.add_dependency 'addressable', '~> 2.2.6'
  spec.add_dependency 'httpi', '~> 2.1.0'
end
