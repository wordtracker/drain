require_relative '../../test_helper'

describe Drain::Config do
  describe '.config' do
    before do
      Drain.config do |config|
        config.app_id = 'app_id'
        config.app_key = 'app_key'
        config.url = 'url'
      end
    end

    after do
      Drain.config do |config|
        config.app_id = nil
        config.app_key = nil
        config.url = nil
      end
    end

    it 'should set global config' do
      assert_equal 'app_id', Drain::Config.app_id
      assert_equal 'app_key', Drain::Config.app_key
      assert_equal 'url', Drain::Config.url
    end

    it 'should allow to create instance without options' do
      pipe = Drain::Pipe.new

      assert_equal 'app_id', pipe.app_id
      assert_equal 'app_key', pipe.app_key
      assert_equal 'url', pipe.url
    end
  end
end
