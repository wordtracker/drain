require_relative '../../test_helper'

describe Drain::Action do
  before do
    pipe = Drain::Pipe.new(app_id: 'id', app_key: 'key', url: 'http://drainpipe')
    @action = Drain::Action.new(pipe)
  end

  describe '#initialize' do
    it 'should initialize the object' do
      assert_instance_of(Drain::Action, @action)
    end
  end

  describe '#auth_tokens' do
    it 'should return auth tokens as a query string' do
      assert_equal('app_id=id&app_key=key', @action.auth_tokens)
    end
  end

  describe '#request_url' do
    it 'should return request url with auth tokens' do
      assert_equal('http://drainpipe/', @action.request_url)
    end
  end

  describe '#handle_response' do
    it 'should raise if code == 400' do
      response = stub(code: 400, body: '{"error": "error"}', error?: true)
      assert_raises(Drain::Error::BadRequest) { @action.handle_response(response) }
    end

    it 'should raise if code == 403' do
      response = stub(code: 403, body: '{"error": "error"}', error?: true)
      assert_raises(Drain::Error::Unauthorized) { @action.handle_response(response) }
    end

    it 'should raise if code == 500' do
      response = stub(code: 500, body: '{"error": "error"}', error?: true)
      assert_raises(Drain::Error::ServerError) { @action.handle_response(response) }
    end

    it 'should return parsed response body' do
      response = stub(code: 200, body: '{"status": "ok"}', error?: false)
      assert_equal({'status' => 'ok'}, @action.handle_response(response))
    end
  end

  describe '#get' do
    it 'should send GET request and return response body' do
      stub_request(:get, 'http://drainpipe/?app_id=id&app_key=key&foo[bar]=baz').with(headers: {'Accept' => '*/*', 'User-Agent' => 'Ruby'}).to_return(body: '{"foo": "bar"}')
      assert_equal({'foo' => 'bar'}, @action.get({:foo => {:bar => 'baz'}}))
    end
  end

  describe '#post' do
    it 'should send POST request and return response body' do
      stub_request(:post, 'http://drainpipe/?app_id=id&app_key=key').with(body: '{"foo":{"bar":"baz"}}', headers: {'Content-Type' => 'application/json'}).to_return(body: '{"foo": "bar"}')
      assert_equal({'foo' => 'bar'}, @action.post({foo: {bar: 'baz'}}))
    end
  end
end
