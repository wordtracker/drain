require_relative '../../test_helper'

describe Drain::Pipe do
  describe '#initialize' do
    it 'should initialize the object' do
      assert_instance_of Drain::Pipe, Drain::Pipe.new(app_id: 'id', app_key: 'token', url: 'http://drainpipe')
    end

    it 'should raise if key is missing' do
      assert_raises(Drain::Error::MissingAppId) { Drain::Pipe.new }
    end

    it 'should raise if token is missing' do
      assert_raises(Drain::Error::MissingAppKey) { Drain::Pipe.new(app_id: 'id') }
    end

    it 'should raise if url is missing' do
      assert_raises(Drain::Error::MissingUrl) { Drain::Pipe.new(app_id: 'id', app_key: 'token') }
    end
  end

  describe '#legacy' do
    before do
      @pipe = Drain::Pipe.new(app_id: 'id', app_key: 'token', url: 'http://drainpipe')
    end

    it 'should convert adults to adult' do
      assert_equal({adult: true}, @pipe.legacy(adults: true))
    end

    it 'should convert adult to adults' do
      assert_equal({adults: ['clean', 'dubious', 'offensive']}, @pipe.legacy(adult: ['clean', 'dubious', 'offensive']))
    end

    it 'should leave adult' do
      assert_equal({adult: true}, @pipe.legacy(adult: true))
    end

    it 'should remove adult' do
      assert_equal({}, @pipe.legacy(adult: false))
    end

    it 'should leave adults' do
      assert_equal({adults: ['clean']}, @pipe.legacy(adults: ['clean']))
    end
  end
end
