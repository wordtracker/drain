require_relative '../../test_helper'

describe Drain::Filter do
  before do
    filter_class = Class.new(Drain::Filter) do
      def perform(input)
        input * input
      end
    end

    @filter = filter_class.new
  end

  describe '#perform' do
    it 'performs filter action on input data' do
      assert_equal 16, @filter.perform(4)
    end
  end

  describe '#apply' do
    it 'returns lambda with encapsulated #perform' do
      input = [1, 2, 3, 4]
      expected = [1, 4, 9, 16]
      assert_equal expected, input.map(&@filter.apply)
    end
  end
end
