require_relative '../../test_helper'

describe Drain::IaatFilter do
  before do
    @filter = Drain::IaatFilter.new
  end

  describe '#perform' do
    it 'competition = 0; kei = 100; when iaat == 0' do
      expected = {
        'competition' => 0,
        'kei' => 100,
        'iaat' => 0
      }

      assert_equal expected, @filter.perform('iaat' => 0)
    end

    it 'competition = -1; kei = -1; when iaat == -1' do
      expected = {
        'competition' => -1,
        'kei' => -1,
        'iaat' => -1
      }

      assert_equal expected, @filter.perform('iaat' => -1)
    end
  end
end
