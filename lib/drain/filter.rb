module Drain
  class Filter

    def perform(input)
      raise 'Not implemented'
    end

    def apply
      lambda { |input| perform(input) }
    end
  end
end
