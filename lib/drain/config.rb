module Drain
  class Config
    @@app_id = nil
    @@app_key = nil
    @@url = nil

    class << self
      def app_id
        @@app_id
      end

      def app_id=(app_id)
        @@app_id = app_id
      end

      def app_key
        @@app_key
      end

      def app_key=(app_key)
        @@app_key = app_key
      end

      def url
        @@url
      end

      def url=(url)
        @@url = url
      end
    end
  end
end
