module Drain
  class Search < Action
    def initialize(*args)
      super
      @filter = Drain::IaatFilter.new
    end

    def perform(parameters = {})
      response = post(parameters)
      response['results'].map!(&@filter.apply)
      response
    end

    def endpoint
      'search'
    end
  end
end
