module Drain
  class CountAll < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'count/all'
    end
  end
end
