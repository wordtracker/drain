module Drain
  class Top < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'top'
    end
  end
end
