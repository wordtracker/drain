require 'httpi'
require 'addressable/uri'
require 'json'

HTTPI.log = false

module Drain
  class Action
    attr_reader :pipe

    def initialize(pipe)
      @pipe = pipe
    end

    def url
      @pipe.url
    end

    def app_id
      @pipe.app_id
    end

    def app_key
      @pipe.app_key
    end

    def auth_tokens
      uri = Addressable::URI.new
      uri.query_values = {app_id: app_id, app_key: app_key}
      uri.query
    end

    def endpoint
      ''
    end

    def request_url
      "#{url}/#{endpoint}"
    end

    def handle_response(response)
      if response.error?
        case response.code
        when 400
          raise Drain::Error::BadRequest.new(response.body)
        when 403
          raise Drain::Error::Unauthorized.new(response.body)
        when 456
          raise Drain::Error::SearchParseError.new
        else
          begin
            body = JSON.parse(response.body)
            message = body['error']
          rescue => ignore
            message = response.body
          end

          raise Drain::Error::ServerError.new(message)
        end
      else
        JSON.parse(response.body)
      end
    end

    def get(parameters = {})
      q = Addressable::URI.new
      q.query_values = parameters

      request = HTTPI::Request.new(request_url)
      request.query = "#{auth_tokens}&#{q.query}"

      response = HTTPI.get(request)
      handle_response(response)
    end

    def post(parameters = {})
      request = HTTPI::Request.new(request_url)

      request.query = auth_tokens
      request.body = JSON.dump(parameters)
      request.headers = {'Content-Type' => 'application/json'}

      response = HTTPI.post(request)
      handle_response(response)
    end

    def perform(parameters = {})
      raise 'Not implemented'
    end
  end
end
