module Drain
  class Pipe
    attr_accessor :app_id, :app_key, :url, :type

    def initialize(options = {})
      @app_id = (options[:app_id] || Drain::Config.app_id) or raise Drain::Error::MissingAppId.new
      @app_key = (options[:app_key] || Drain::Config.app_key) or raise Drain::Error::MissingAppKey.new
      @url = (options[:url] || Drain::Config.url) or raise Drain::Error::MissingUrl.new
    end

    def count(parameters = {})
      action = Count.new(self)
      action.perform(parameters)
    end

    def count_all(parameters = {})
      action = CountAll.new(self)
      action.perform(parameters)
    end

    def fetch(parameters = {})
      action = Fetch.new(self)
      action.perform(parameters)
    end

    def fetch_stats(parameters = {})
      action = FetchStats.new(self)
      action.perform(parameters)
    end

    def periods
      action = Periods.new(self)
      action.perform
    end

    def countries(parameters = {})
      action = Countries.new(self)
      action.perform(parameters)
    end

    def search(parameters = {})
      action = Search.new(self)
      action.perform(parameters)
    end

    def search_stats(parameters = {})
      action = SearchStats.new(self)
      action.perform(parameters)
    end

    def stats(parameters = {})
      action = Stats.new(self)
      action.perform(parameters)
    end

    def top(parameters = {})
      action = Top.new(self)
      action.perform(parameters)
    end

    def metrics
      ['volume', 'iaat', 'competition', 'kei']
    end

    def legacy(parameters = {})
      adult = parameters[:adult] || parameters[:adults]

      parameters.delete(:adult)
      parameters.delete(:adults)

      if adult.is_a?(Array)
        parameters[:adults] = adult
      else
        parameters[:adult] = adult unless adult.nil?
      end

      parameters
    end

    def match(keywords, parameters = {})
      defaults = {keywords: keywords, type: 'broad', metrics: metrics}
      params = defaults.merge(legacy(parameters))
      res = search(params)
      # res['results']
      res
    end

    def phrase(keywords, parameters = {})
      params = legacy(parameters).merge(keywords: keywords, type: 'phrase', metrics: metrics)
      res = search(params)
      # res['results']
      res
    end

    def exact(keywords, parameters = {})
      params = legacy(parameters).merge(keywords: keywords, metrics: metrics)

      params.delete(:adult)
      params.delete(:adults)

      res = fetch(params)
      # res['results']
      res
    end

    def remove_adult_terms(keywords)
      results = exact(keywords, {adult: true, limit: keywords.size})
      results.select do |keyword|
        ['CLEAN', 'DUBIOUS'].include?(keyword['adult'])
      end
    end
  end
end
