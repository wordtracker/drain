module Drain
  class Count < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'count'
    end
  end
end
