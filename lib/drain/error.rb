module Drain
  module Error
    class MissingAppId < ArgumentError; end
    class MissingAppKey < ArgumentError; end
    class MissingUrl < ArgumentError; end
    class Unauthorized < StandardError; end
    class BadRequest < StandardError; end
    class ServerError < StandardError; end
    class SearchParseError < StandardError; end
  end
end
