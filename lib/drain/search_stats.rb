module Drain
  class SearchStats < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'search/stats'
    end
  end
end
