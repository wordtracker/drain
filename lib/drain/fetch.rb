module Drain
  class Fetch < Action
    def initialize(*args)
      super
      @filter = Drain::IaatFilter.new
    end

    def perform(parameters = {})
      response = post(parameters)
      response['results'] = response['results'].map(&@filter.apply)
      response
    end

    def endpoint
      'fetch'
    end
  end
end
