module Drain
  class Stats < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'stats'
    end
  end
end
