module Drain
  class IaatFilter < Filter
    def perform(input)

      case input['iaat']
      when 0
        input['competition'] = 0
        input['kei'] = 100
      when -1
        input['competition'] = -1
        input['kei'] = -1
      end

      input
    end
  end
end
