module Drain
  class FetchStats < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'fetch/stats'
    end
  end
end
