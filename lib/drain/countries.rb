module Drain
  class Countries < Action
    def perform(parameters = {})
      post(parameters)
    end

    def endpoint
      'countries'
    end
  end
end
