require 'drain/error'
require 'drain/filter'
require 'drain/iaat_filter'

require 'drain/action'
require 'drain/count'
require 'drain/count_all'
require 'drain/fetch'
require 'drain/fetch_stats'
require 'drain/periods'
require 'drain/countries'
require 'drain/search'
require 'drain/search_stats'
require 'drain/stats'
require 'drain/top'

require 'drain/pipe'
require 'drain/version'
require 'drain/config'

module Drain
  def self.config(&block)
    block.call(Drain::Config)
  end
end
